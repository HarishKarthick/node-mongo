module.exports = (app) => {
  var userController = require('../controller/userController.js')
  app.get('/getUser', userController.getUserData);
  app.post('/createUser', userController.create);
  app.get('/getallUserData', userController.getallUserData);
  app.put('/updateUser', userController.updateUser);
  app.post('/deletUser', userController.deletUser);
  app.post('/login', userController.login);
  app.get('/loginList', userController.loginList);
}
