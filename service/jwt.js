var jwt = require('jsonwebtoken');
var secret = 'liveapp';
module.exports = {
    generate : async  (obj) => {
        var token = jwt.sign(obj, secret);
        return token;
    },
    verify : async (token) => {
        token = token.replace('Bearer ', "");
        var verify = jwt.verify(token, secret);
        return verify;
    }
}