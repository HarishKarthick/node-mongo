var bcrypt = require('bcryptjs');
module.exports = {
    paswordGenerate : async (password) => {
        const hash = bcrypt.hashSync(password, 10);
        if (hash) {
            return hash;
        } else {
            return null
        }
    },
    passwordVerify : async (password , dbPassword) => {
        var check = bcrypt.compareSync(password, dbPassword);
        console.log(check)
        return check;

    }
}