var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var UserSchema = mongoose.Schema({
	userName : String,
	password : String,
	email : String,
	mobile : String,
	role : {
		type : Schema.Types.ObjectId,
		ref : "role"
	}
},{
	timestamp: true
})
module.exports = mongoose.model('User', UserSchema);