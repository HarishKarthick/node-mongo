var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const opts = {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
};

var LoginStatus = mongoose.Schema({
    User : {
        type : Schema.Types.ObjectId,
        ref : 'User'
    },
    role : {
        type : Schema.Types.ObjectId,
        ref : 'role'
    },
    logOnTime : { type: Date, default: Date.now }
},opts);

module.exports = mongoose.model('LoginStatus', LoginStatus);