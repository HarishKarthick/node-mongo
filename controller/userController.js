// const User = require('../model/userModel.js');
var db = require("../model");
const LoginStatus = require("../model/LoginStatus");
const jwt = require('../service/jwt.js');
const PasswordService = require('../service/password.js')
module.exports = {
  getUserData : async (req, res) => {
    const email  = req.param('email');
    var result = await db.User.findOne({email: email});
    res.send({
      statusCode : 200,
      apiStatus: true,
      result : result
    })
  },
  getallUserData : async (req, res) => {
    const email  = req.param('email');
    var result = await db.User.find();
    res.send({
      statusCode : 200,
      apiStatus: true,
      result : result
    })
  },
  create : async (req,res) => {
    const { email, userName, password, mobile } = req.body;
    var hash = await PasswordService.paswordGenerate(password)
    var check = await db.User.count({email : email});
    console.log(check);
    if (check){
      return res.send({message : "Email already exist"});
    }
    console.log(hash);
    var user = new db.User({
      userName : userName,
      email : email,
      password : hash,
      mobile: mobile
    });
    var result = await user.save();
    res.send({statusCode : 200,apiStatus: true, "result" : result});
  },
  updateUser : async(req,res) => {
    const {email , userName, password} = req.body;
    if(!email)
      return res.send({statusCode : 400, apiStatus: false, message: "email is madatory"});
    if(!userName && !password){
      return res.send({statusCode : 400, apiStatus: false, message: "Please pass the update values"});
    }
    var createQuery = {};
    if(userName){
      createQuery.userName = userName;
    }
    if(password){
      var hash = await PasswordService.paswordGenerate(password);
      createQuery.password = hash
    }
    console.log(createQuery)
    var UpdateData = await db.User.updateMany({email : email} , createQuery);
    console.log(UpdateData);
    res.send({
      statusCode:200,
      apiStatus: true,
      result: UpdateData,
      message : "updated successfully"
    })
  },
  deletUser : async(req, res) => {
    const {email} = req.body;
    var Ddata = await db.User.deleteMany({email: email});
    console.log(Ddata);
    res.send(Ddata)
  },
  login : async (req, res) => {
    const {email, password} = req.body;
    var emailVerify = await (await db.User.findOne({email: email})).populate('role')
    if (!emailVerify){
      return res.send({apiStatus: false, statusCode: 400, message: "User Not found"});
    }
    var check = await PasswordService.passwordVerify(password, emailVerify.password);
    if (check == false){
      return res.send({apiStatus: false, statusCode: 400, message: "password is wrong"});
    }
    var obj = {
      User : emailVerify._id,
    };
    var loginState = db.LoginStatus(obj);
    var storeLogin = await loginState.save();
    var token = await jwt.generate({id : emailVerify._id, email : emailVerify.email});
    res.send({
      statusCode : 200,
      apiStatus: true,
      result : emailVerify,
      token : token
    });
  },
  loginList : async (req,res) => {
    if(!req.headers && !req.headers.authorization){
      return res.send({apiStatus : false, statusCode: 401, message : "You Dont't have access"});
    }
    var data = await jwt.verify(req.headers.authorization);
    var logStatus = await LoginStatus.find({User : data.id}).populate('User');
    res.send({apiStatus:true, statusCode: 200, result : logStatus});
  }
}
