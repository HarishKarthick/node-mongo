var express = require('express');
const bodyParser = require('body-parser');
var app = express();

const dbConfig = require('./config/config.js');
const mongoose = require('mongoose');
var routes = require('./routes/routes.js')

mongoose.Promise = global.Promise;


app.use(bodyParser.urlencoded({ extended: true }));

app.use(bodyParser.json());

routes(app);

mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.listen(5050, () => {
    console.log("Server is listening on port 5050");
});